//
//  ViewController.m
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property UITextField * longitude;
@property UITextField * latitude;
@property UITextField * horizontalAccuracy;

@property UITextField * pinTitle;
@property UITextField * pinSubTitle;
@property UITextView  * notes;
@property CLLocationCoordinate2D coordinate;

@property UIStackView * stack;
@property UIStackView * buttonStackViews;

typedef enum {
    mapOnly,
    mapWithTitles,
    mapWithTitlesAndNotes,
    mapWithNotesOnly
} mapEditOptions;

@property mapEditOptions mapEditingOption;

@end

@implementation ViewController

- (void) recursiveAnnimationWithButtons: (NSArray*) buttons index: (NSInteger) pIndex
{
    __block NSInteger index = pIndex;
    [UIView animateWithDuration: 0.1
                     animations: ^{
                         UIButton * button = [buttons objectAtIndex: index];
                         button.hidden = NO;}
                     completion: ^(BOOL finished) {
                         if (index > buttons.count - 2) {
                             finished = YES;
                         }
                         else {
                             index++;
                             [self recursiveAnnimationWithButtons: buttons index: index];
                         }
                     }
     ];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    NSArray * buttons = [self.buttonStackViews arrangedSubviews];
    [self recursiveAnnimationWithButtons: buttons index: 0];
}


- (void)viewDidLoad {
    [super viewDidLoad];


    // Oh hell, let's try out stackviews.
    // If done well, not frames needed.
    
    [self setupText];
    [self setupButtons];

    // Add you favorite default location.
    _horizontalAccuracy.text = [NSString stringWithFormat: @"%f", 5.0];
    NSInteger pickLocation = 0;
    switch (pickLocation) {
        case 1:
            // Osoyoos B.C. Canada
            _latitude.text = [NSString stringWithFormat: @"%f", 49.030992];
            _longitude.text = [NSString stringWithFormat: @"%f", -119.463052];
            break;
        case 2:
            // Yellowstone NP, U.S.A. - Grand Prismatic Spring
            _latitude.text = [NSString stringWithFormat: @"%f", 44.5256811];
            _longitude.text = [NSString stringWithFormat: @"%f", -110.8381345];
            break;
            
        default:
            // Vancouver B.C. Canada
            _latitude.text = [NSString stringWithFormat: @"%f", 49.270218];
            _longitude.text = [NSString stringWithFormat: @"%f", -123.135871];
            break;
    }
}


- (void) setupText
{
    UILabel * msgLabel = [UILabel new];
    UILabel * latitudeLabel = [UILabel new];
    UILabel * longitudeLabel = [UILabel new];
    UILabel * horizontalAccuracyLabel = [UILabel new];
    
    
    
    _latitude = [UITextField new];
    _longitude = [UITextField new];
    _horizontalAccuracy = [UITextField new];
    
    
    msgLabel.translatesAutoresizingMaskIntoConstraints = NO;
    latitudeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    longitudeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    horizontalAccuracyLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _latitude.translatesAutoresizingMaskIntoConstraints = NO;
    _longitude.translatesAutoresizingMaskIntoConstraints = NO;
    _horizontalAccuracy.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    NSMutableArray * stackSubviews = [[NSMutableArray alloc] init];
    NSArray * tempStackSubviews = nil;
    UIStackView * tempStack = nil;
    
    msgLabel.textAlignment = NSTextAlignmentCenter; // Have to center label text separately from box end result.
    tempStackSubviews =  @[msgLabel];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    tempStack.distribution = UIStackViewDistributionEqualCentering;
    tempStack.alignment = UIStackViewAlignmentCenter;
    [stackSubviews addObject: tempStack];
    
    CGFloat someConstant = 75.0; // Because I want to limit the width of the two labels.
    
    tempStackSubviews =  @[latitudeLabel, _latitude];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    [latitudeLabel.widthAnchor constraintLessThanOrEqualToAnchor: _latitude.widthAnchor constant: - someConstant].active = true;
    [latitudeLabel.widthAnchor constraintGreaterThanOrEqualToAnchor: nil constant: someConstant].active = true;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentLeading;
    tempStack.spacing = 8;
    [stackSubviews addObject: tempStack];
    
    tempStackSubviews = @[longitudeLabel, _longitude];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    [longitudeLabel.widthAnchor constraintLessThanOrEqualToAnchor: _longitude.widthAnchor constant: - someConstant].active = true;
    [longitudeLabel.widthAnchor constraintGreaterThanOrEqualToAnchor: nil constant: someConstant].active = true;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentFirstBaseline;
    tempStack.spacing = 8;
    [stackSubviews addObject: tempStack];

    tempStackSubviews = @[horizontalAccuracyLabel, _horizontalAccuracy];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    [horizontalAccuracyLabel.widthAnchor constraintLessThanOrEqualToAnchor: _horizontalAccuracy.widthAnchor constant: - someConstant].active = true;
    [horizontalAccuracyLabel.widthAnchor constraintGreaterThanOrEqualToAnchor: nil constant: someConstant].active = true;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentFirstBaseline;
    tempStack.spacing = 8;
    [stackSubviews addObject: tempStack];

    
    
    self.stack = [[UIStackView alloc] initWithArrangedSubviews: stackSubviews];
    self.stack.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview: self.stack];
    [self.stack.widthAnchor constraintLessThanOrEqualToAnchor: nil constant: 300.0].active = true;
    [self.stack.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = true;
    [self.stack.topAnchor constraintEqualToAnchor: self.view.topAnchor constant: 40.0].active = true;
    [self.stack.leadingAnchor constraintGreaterThanOrEqualToAnchor: self.view.leadingAnchor constant: 10.0].active = true;
    [self.stack.trailingAnchor constraintLessThanOrEqualToAnchor: self.view.trailingAnchor constant: -10.0].active = true;
    [self.stack.bottomAnchor constraintLessThanOrEqualToAnchor: self.view.bottomAnchor constant: -5.0].active = true;
    self.stack.axis = UILayoutConstraintAxisVertical;
    self.stack.distribution = UIStackViewDistributionFillEqually;
    //stack.alignment = UIStackViewAlignmentFirstBaseline;  // With the above, this messes up alignment.
    self.stack.spacing = 8; // If you set this, then don't mess with top & bottom anchors.
    
    
    // Ensures enforcement that labels are equal widths because stacks are otherwise tricky!
    [latitudeLabel.widthAnchor constraintEqualToAnchor: longitudeLabel.widthAnchor constant: 1.0].active = true;
    [latitudeLabel.widthAnchor constraintEqualToAnchor: horizontalAccuracyLabel.widthAnchor constant: 1.0].active = true;
    
    
    
    UIView * stackBackgroundColorView = [UIView new];
    stackBackgroundColorView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    stackBackgroundColorView.layer.borderWidth = 1.0;
    stackBackgroundColorView.layer.cornerRadius = 6.0;
    [self.view insertSubview: stackBackgroundColorView belowSubview: self.stack];
    stackBackgroundColorView.translatesAutoresizingMaskIntoConstraints = NO;
//    stackBackgroundColorView.backgroundColor = [UIColor redColor];
    
    [stackBackgroundColorView.topAnchor constraintEqualToAnchor: self.self.stack.topAnchor constant: -5.0].active = true;
    [stackBackgroundColorView.leadingAnchor constraintEqualToAnchor: self.stack.leadingAnchor constant: -5.0].active = true;
    [stackBackgroundColorView.trailingAnchor constraintEqualToAnchor: self.stack.trailingAnchor constant: 5.0].active = true;
    [stackBackgroundColorView.bottomAnchor constraintEqualToAnchor: self.stack.bottomAnchor constant: 5.0].active = true;
    
    // Had to use top, leading, trailing & bottom anchors instead of centers & width & height because need 'constant'.
    //    [stackBackgroundColorView.centerXAnchor constraintEqualToAnchor: stack.centerXAnchor].active = true;
    //    [stackBackgroundColorView.centerYAnchor constraintEqualToAnchor: stack.centerYAnchor].active = true;
    //    [stackBackgroundColorView.widthAnchor constraintEqualToAnchor: stack.widthAnchor].active = true;
    //    [stackBackgroundColorView.heightAnchor constraintEqualToAnchor: stack.heightAnchor].active = true;
    
    
    
    msgLabel.text = @"Sample VC";
    latitudeLabel.text = @"Latitude:";
    longitudeLabel.text = @"Longitude:";
    horizontalAccuracyLabel.text = @"Accuracy";
    _latitude.backgroundColor = [UIColor lightGrayColor];
    _longitude.backgroundColor = [UIColor lightGrayColor];
    _horizontalAccuracy.backgroundColor = [UIColor lightGrayColor];
}


- (void) setupButtons
{
    NSMutableArray * buttonsArray = [NSMutableArray new];

    UIButton * adjustmentButton1 = [UIButton buttonWithType: UIButtonTypeSystem];
    adjustmentButton1.translatesAutoresizingMaskIntoConstraints = NO;
    adjustmentButton1.hidden = YES;
    [buttonsArray addObject: adjustmentButton1];
    [adjustmentButton1 setTitle: @"Map Only" forState: UIControlStateNormal];
//    adjustmentButton1.backgroundColor = [UIColor purpleColor];
    [adjustmentButton1 addTarget: self action: @selector(openPinView1) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton * adjustmentButton2 = [UIButton buttonWithType: UIButtonTypeSystem];
    adjustmentButton2.translatesAutoresizingMaskIntoConstraints = NO;
    adjustmentButton2.hidden = YES;
    [buttonsArray addObject: adjustmentButton2];
    [adjustmentButton2 setTitle: @"Map with Titles" forState: UIControlStateNormal];
//    adjustmentButton2.backgroundColor = [UIColor purpleColor];
    [adjustmentButton2 addTarget: self action: @selector(openPinView2) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton * adjustmentButton3 = [UIButton buttonWithType: UIButtonTypeSystem];
    adjustmentButton3.translatesAutoresizingMaskIntoConstraints = NO;
    adjustmentButton3.hidden = YES;
    [buttonsArray addObject: adjustmentButton3];
    [adjustmentButton3 setTitle: @"Map with Notes" forState: UIControlStateNormal];
//    adjustmentButton3.backgroundColor = [UIColor purpleColor];
    [adjustmentButton3 addTarget: self action: @selector(openPinView3) forControlEvents: UIControlEventTouchUpInside];
    
    UIButton * adjustmentButton4 = [UIButton buttonWithType: UIButtonTypeSystem];
    adjustmentButton4.translatesAutoresizingMaskIntoConstraints = NO;
    adjustmentButton4.hidden = YES;
    [buttonsArray addObject: adjustmentButton4];
    [adjustmentButton4 setTitle: @"Map with Notes Only" forState: UIControlStateNormal];
//    adjustmentButton4.backgroundColor = [UIColor purpleColor];
    [adjustmentButton4 addTarget: self action: @selector(openPinView4) forControlEvents: UIControlEventTouchUpInside];
    
    self.buttonStackViews = [[UIStackView alloc] initWithArrangedSubviews: buttonsArray];
    self.buttonStackViews.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview: self.buttonStackViews];
    self.buttonStackViews.axis = UILayoutConstraintAxisVertical;
    self.buttonStackViews.distribution = UIStackViewDistributionFillEqually;
    self.buttonStackViews.spacing = 8;

    [self.buttonStackViews.topAnchor constraintEqualToAnchor: self.stack.bottomAnchor constant: 20.0].active = true;
    [self.buttonStackViews.trailingAnchor constraintEqualToAnchor: self.view.centerXAnchor constant: -5.0].active = true;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DJRAdjustPinMapViewController Call
- (void) openPinView1
{
    _mapEditingOption = mapOnly;

    _coordinate.latitude = [_latitude.text floatValue];
    _coordinate.longitude = [_longitude.text floatValue];
    CGFloat horizontalAccuracy = [_horizontalAccuracy.text floatValue];
    
    DJRAdjustPinMapViewController * mapCntrlr = [[DJRAdjustPinMapViewController alloc] initWithCoordinate: _coordinate
                                                                                       horizontalAccuracy: horizontalAccuracy];
    mapCntrlr.delegate = self;
    mapCntrlr.modalPresentationStyle = UIModalPresentationFormSheet; // UIModalPresentationPageSheet;
    [self presentViewController: mapCntrlr
                       animated: YES
                     completion: nil];
}


- (void) openPinView2
{
    _mapEditingOption = mapWithTitles;
    
    _coordinate.latitude = [_latitude.text floatValue];
    _coordinate.longitude = [_longitude.text floatValue];
    CGFloat horizontalAccuracy = [_horizontalAccuracy.text floatValue];

    DJRAdjustPinMapViewController * mapCntrlr =
    [[DJRAdjustPinMapViewController alloc] initWithCoordinate: _coordinate
                                           horizontalAccuracy: horizontalAccuracy
                                                        title: @"test title"
                                                     subTitle: @"test subtitle"];
    mapCntrlr.delegate = self;
    mapCntrlr.modalPresentationStyle = UIModalPresentationFormSheet; // UIModalPresentationPageSheet;
    [self presentViewController: mapCntrlr
                       animated: YES
                     completion: nil];
}


- (void) openPinView3
{
    _mapEditingOption = mapWithTitlesAndNotes;
    
    _coordinate.latitude = [_latitude.text floatValue];
    _coordinate.longitude = [_longitude.text floatValue];
    CGFloat horizontalAccuracy = [_horizontalAccuracy.text floatValue];

    DJRAdjustPinMapViewController * mapCntrlr =
    [[DJRAdjustPinMapViewController alloc] initWithCoordinate: _coordinate
                                           horizontalAccuracy: horizontalAccuracy
                                                        title: @"test 2 title"
                                                     subTitle: @"test 2 subtitle"
                                                        notes: @"test notes"];

    
    // Manipulation of the labels and text fields is possible, but see below for further details.
    // ==================================================
    mapCntrlr.pinTitleLabel.text = @"tite";
    mapCntrlr.pinSubTitleLabel.text = @"sub tite";
    mapCntrlr.pinNotesLabel.text = @"desc";
    NSArray * viewsToManipulate = @[mapCntrlr.pinTitle, mapCntrlr.pinSubTitle, mapCntrlr.pinNotes];
    for (UIView * view in viewsToManipulate) {
        view.layer.cornerRadius = 6.0;
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [UIColor colorWithRed: 0.0 green: 0.5 blue: 0.0 alpha: 0.5].CGColor;
    }
    mapCntrlr.pinTitle.enabled = NO;
    mapCntrlr.pinSubTitle.clearButtonMode = UITextFieldViewModeUnlessEditing;
    // ==================================================
    
    
    mapCntrlr.delegate = self;
    mapCntrlr.modalPresentationStyle = UIModalPresentationFormSheet; // UIModalPresentationPageSheet;
    [self presentViewController: mapCntrlr
                       animated: YES
                     completion: nil];
    
    
    // Manipulation of the background color of fields must come after presention.
    // ==================================================
    CGFloat colorValue = 0.9; // A light blue-gray that allow us to see the white mapped edges.
    UIColor * backColor = [UIColor colorWithRed: colorValue - 0.2 green: colorValue - 0.1 blue: colorValue alpha: 1.0];
    for (UIView * view in viewsToManipulate) {
        view.layer.backgroundColor = backColor.CGColor;
    }
    // Yep, even this, 😁 and I suppose further auto layout regarding size, and font changes.
    mapCntrlr.pinTitle.layer.sublayerTransform = CATransform3DMakeTranslation(3, 0, 0);
    mapCntrlr.pinSubTitle.layer.sublayerTransform = CATransform3DMakeTranslation(3, 0, 0);
    // ==================================================
}

- (void) openPinView4
{
    _mapEditingOption = mapWithNotesOnly;
    
    _coordinate.latitude = [_latitude.text floatValue];
    _coordinate.longitude = [_longitude.text floatValue];
    CGFloat horizontalAccuracy = [_horizontalAccuracy.text floatValue];
    
    DJRAdjustPinMapViewController * mapCntrlr =
    [[DJRAdjustPinMapViewController alloc] initWithCoordinate: _coordinate
                                           horizontalAccuracy: horizontalAccuracy
                                                        notes: @"only test notes"];
    
    mapCntrlr.delegate = self;
    mapCntrlr.modalPresentationStyle = UIModalPresentationFormSheet; // UIModalPresentationPageSheet;
    [self presentViewController: mapCntrlr
                       animated: YES
                     completion: nil];
}



#pragma mark - DJRAdjustPinMapViewControllerDelegates
- (void) updateLocationCoordinate: (DJRAdjustPinMapViewController *) adjustPinMapViewController
{
    _latitude.text = [NSString stringWithFormat: @"%f", adjustPinMapViewController.pinCoordinate.latitude];
    _longitude.text = [NSString stringWithFormat: @"%f", adjustPinMapViewController.pinCoordinate.longitude];
    _horizontalAccuracy.text = [NSString stringWithFormat: @"%f", adjustPinMapViewController.horizontalAccuracy];

    
    // Further screen feedback left to the guest coder. Maybe nil of not passed to mapVC.
    if (_mapEditingOption == mapWithTitles || _mapEditingOption == mapWithTitlesAndNotes) {
        NSLog(@"Pin title: %@", adjustPinMapViewController.pinTitle);
        NSLog(@"Pin subtitle: %@", adjustPinMapViewController.pinSubTitle);
    }
    
    if (_mapEditingOption == mapWithNotesOnly || _mapEditingOption == mapWithTitlesAndNotes) {
        NSLog(@"Pin notes: %@", adjustPinMapViewController.pinNotes);
    }
}

- (void) cancelLocationCoordinateUpdate
{
    //NSLog(@"cancelLocationCoordinateUpdate");
}


@end
