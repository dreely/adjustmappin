//
//  ViewController.h
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

@import UIKit;
#import "DJRAdjustPinMapView.h"
#import "DJRAdjustPinMapViewController.h"

@interface ViewController : UIViewController <DJRAdjustPinMapViewControllerDelegate>

- (void) updateLocationCoordinate: (DJRAdjustPinMapViewController *) adjustPinMapViewController;
- (void) cancelLocationCoordinateUpdate;

@end

