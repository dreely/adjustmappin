//
//  DJRAdjustPinMapViewController.m
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

#import "DJRAdjustPinMapViewController.h"


@interface DJRAdjustPinMapViewController ()
@property (strong, nonatomic) IBOutlet UIButton * cancelButton;
@property (strong, nonatomic) IBOutlet UIButton * saveButton;

@property (strong, nonatomic) IBOutlet DJRAdjustPinMapView *mapview;

@property BOOL displayCancelAndSaveButtons;
@property BOOL displayTitles;
@property BOOL displayNotes;

// Using int's in the autolayout visual format language strings reduces work and warnings.
@property int verticalOffset;
@property int borderInset;
@property CGFloat maxLabelWidth;

@property UIStackView * stack;

@end


@implementation DJRAdjustPinMapViewController

#pragma mark - Initializers

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
{
    self = [super init];
    if (self) {
        _pinCoordinate = pCoordinate;
        _horizontalAccuracy = pHorizontalAccuracy;
        _displayCancelAndSaveButtons = YES;
        
        _displayTitles = NO;
        _displayNotes = NO;
    }
    return self;
}


- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              title: (NSString*) pTitle
                           subTitle: (NSString*) pSubTitle
{
    self = [self initWithCoordinate: pCoordinate horizontalAccuracy: pHorizontalAccuracy];
    if (self) {
        _pinTitleLabel = [UILabel new];
        _pinTitleLabel.text = @"Title:";
        _pinTitle = [UITextField new];
        _pinTitle.placeholder = @"title";
        
        _pinSubTitleLabel = [UILabel new];
        _pinSubTitleLabel.text = @"Subtitle:";
        _pinSubTitle = [UITextField new];
        _pinSubTitle.placeholder = @"subtitle";
        
        _pinTitle.text = pTitle;
        _pinSubTitle.text = pSubTitle;
        _displayTitles = YES;
    }
    return self;
}

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              title: (NSString*) pTitle
                           subTitle: (NSString*) pSubTitle
                              notes: (NSString*) pNotes
{
    self = [self initWithCoordinate: pCoordinate
                 horizontalAccuracy: pHorizontalAccuracy
                              title: pTitle
                           subTitle: pSubTitle];
    if (self) {
        _pinNotesLabel = [UILabel new];
        _pinNotesLabel.text = @"Notes:";
        _pinNotes = [UITextView new];
        _pinNotes.text = pNotes;
        _displayNotes = YES;
    }
    return self;
}

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              notes: (NSString*) pNotes
{
    self = [self initWithCoordinate: pCoordinate horizontalAccuracy: pHorizontalAccuracy];
    if (self) {
        _pinNotesLabel = [UILabel new];
        _pinNotesLabel.text = @"Notes:";
        _pinNotes = [UITextView new];
        _pinNotes.text = pNotes;
        _displayNotes = YES;
    }
    return self;
}



#pragma mark - Layout Setup
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _verticalOffset = 6;
    _borderInset = 15;
    _maxLabelWidth = 100.0;
    
    // The presenter seems to be forcing this to an opaque color, so don't use alpha.
    CGFloat colorValue = 0.9; // A very light gray that allow us to see the white mapped edges.
    self.view.backgroundColor = [UIColor colorWithRed: colorValue green: colorValue blue: colorValue alpha: 1.0];
    
    if (_displayCancelAndSaveButtons) {
        _cancelButton = [UIButton buttonWithType: UIButtonTypeSystem];
        [_cancelButton setTitle: @"Cancel" forState: UIControlStateNormal];
        [_cancelButton addTarget: self
                          action: @selector(cancelUpdateOfDelegateLocation:)
                forControlEvents: UIControlEventTouchUpInside];
        
        _saveButton = [UIButton buttonWithType: UIButtonTypeSystem];
        [_saveButton setTitle: @"Save" forState: UIControlStateNormal];
        [_saveButton addTarget: self
                        action: @selector(updateDelegateLocation:)
              forControlEvents: UIControlEventTouchUpInside];
        [self.view addSubview: _cancelButton];
        [self.view addSubview: _saveButton];
    }
    
    _stack = [UIStackView new];
    _stack.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview: _stack];
    [self.stack.widthAnchor constraintLessThanOrEqualToAnchor: nil constant: 500.0].active = true;
    
    CGFloat minWidth = 375;
    if (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        CGRect windowBounds = self.view.bounds;
        minWidth = windowBounds.size.width < windowBounds.size.height ? windowBounds.size.width : windowBounds.size.height;
        minWidth = minWidth - (15.0 * 2.0); // 15 appears to be the default in VFL language for leading edges.
    }
    [self.stack.widthAnchor constraintGreaterThanOrEqualToAnchor: nil constant: minWidth].active = true;
    
    [self.stack.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = true;
    [self.stack.leadingAnchor constraintGreaterThanOrEqualToAnchor: self.view.leadingAnchor constant: 15.0].active = true;
    [self.stack.trailingAnchor constraintLessThanOrEqualToAnchor: self.view.trailingAnchor constant: -15.0].active = true;
    [self.stack.bottomAnchor constraintLessThanOrEqualToAnchor: self.view.bottomAnchor constant: -5.0].active = true;
    self.stack.axis = UILayoutConstraintAxisVertical;
    self.stack.distribution = UIStackViewDistributionFill;
    self.stack.spacing = 8;
    
    _mapview = [[DJRAdjustPinMapView alloc] initWithCorordiate2D: _pinCoordinate];
    _mapview.consumerDelegate = self;
    _mapview.showsCompass = YES;
    _mapview.showsScale = YES;
    _mapview.showsBuildings = YES;
    _mapview.showsPointsOfInterest = YES;
    [self.view addSubview: _mapview];
    
    [self layoutViews];
    
    // Late because buttons will not be part of view hierarchy until after layoutViews is called.
    if (_displayCancelAndSaveButtons) {
        [self.stack.topAnchor constraintEqualToAnchor: _cancelButton.bottomAnchor constant: 15.0].active = true;
    }
    else {
        [self.stack.topAnchor constraintEqualToAnchor: self.view.topAnchor constant: 40.0].active = true;
    }
}


#pragma mark - AutoLayout Setup
- (void) layoutViews
{
    UIView * aboveView = nil;
    
    if (_displayCancelAndSaveButtons) {
        [self layoutCancelAndSaveButtons];
        aboveView = _cancelButton;
    }
    
    if (_displayTitles) {
        [self layoutTitlesFromAboveView: aboveView];
        aboveView = _stack;
    }
    
    if (_displayNotes) {
        [self layoutPinNotesFromAboveView: aboveView];
        aboveView = _stack;
    }
    
    // Setup the map layout
    NSArray * returnConstraintsArray = nil;
    NSMutableDictionary * myViews = nil;
    
    _mapview.translatesAutoresizingMaskIntoConstraints = NO;
    returnConstraintsArray = [NSArray new];
    myViews = [NSMutableDictionary new];
    [myViews setObject: _mapview forKey: @"mapview"];
    
    NSString * constraintVFL = @"H:|-[mapview]-|"; // Defaults gap to 15
    NSArray * lConstraints = [NSLayoutConstraint
                              constraintsWithVisualFormat: constraintVFL
                              options: 0
                              metrics: nil
                              views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    
    NSString * defaultVerticalLayout = [NSString stringWithFormat: @"-%i-[mapview]-%i-|", _borderInset, _borderInset];
    
    if (!aboveView) {
        // Weird - Top and bottom defaulted to zero.
        constraintVFL = [NSString stringWithFormat: @"V:|%@", defaultVerticalLayout];
    }
    else {
        [myViews setObject: aboveView forKey: @"aboveView"];
        constraintVFL = [NSString stringWithFormat: @"V:[aboveView]%@", defaultVerticalLayout];
    }
    
    lConstraints = [NSLayoutConstraint
                    constraintsWithVisualFormat: constraintVFL
                    options: 0
                    metrics: nil
                    views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    [self.view addConstraints: returnConstraintsArray];
}

- (void) layoutCancelAndSaveButtons
{
    _saveButton.translatesAutoresizingMaskIntoConstraints = NO;
    _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray* returnConstraintsArray = [NSArray new];
    NSMutableDictionary * myViews = [NSMutableDictionary new];
    [myViews setObject: _saveButton forKey: @"saveview"];
    
    NSString * constraintVFL = [NSString stringWithFormat: @"H:[saveview]-%i-|", _borderInset];
    NSArray * lConstraints = [NSLayoutConstraint
                              constraintsWithVisualFormat: constraintVFL
                              options: 0
                              metrics: nil
                              views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    
    constraintVFL = [NSString stringWithFormat: @"V:|-%i-[saveview]", _verticalOffset * 3]; // Top and bottom defaulting to zero.
    lConstraints = [NSLayoutConstraint
                    constraintsWithVisualFormat: constraintVFL
                    options: 0
                    metrics: nil
                    views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    [self.view addConstraints: returnConstraintsArray];
    
    
    returnConstraintsArray = [NSArray new];
    myViews = [NSMutableDictionary new];
    [myViews setObject: _cancelButton forKey: @"cancelview"];
    
    constraintVFL = [NSString stringWithFormat: @"H:|-%i-[cancelview]", _borderInset];
    lConstraints = [NSLayoutConstraint
                    constraintsWithVisualFormat: constraintVFL
                    options: 0
                    metrics: nil
                    views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    constraintVFL = [NSString stringWithFormat: @"V:|-%i-[cancelview]", _verticalOffset * 3]; // Top and bottom defaulting to zero.
    lConstraints = [NSLayoutConstraint
                    constraintsWithVisualFormat: constraintVFL
                    options: 0
                    metrics: nil
                    views: myViews];
    returnConstraintsArray = [returnConstraintsArray arrayByAddingObjectsFromArray: lConstraints];
    [self.view addConstraints: returnConstraintsArray];
}

- (void) layoutTitlesFromAboveView: (UIView*) aboveView
{
    NSArray * tempStackSubviews = nil;
    UIStackView * tempStack = nil;
    _pinTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _pinTitle.translatesAutoresizingMaskIntoConstraints = NO;
    _pinSubTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _pinSubTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    tempStackSubviews =  @[_pinTitleLabel, _pinTitle];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    tempStack.translatesAutoresizingMaskIntoConstraints = NO;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentLeading;
    tempStack.spacing = 8;
    [_stack addArrangedSubview: tempStack];
    
    tempStackSubviews =  @[_pinSubTitleLabel, _pinSubTitle];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    tempStack.translatesAutoresizingMaskIntoConstraints = NO;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentLeading;
    tempStack.spacing = 8;
    [_stack addArrangedSubview: tempStack];
    
    [_pinTitleLabel.widthAnchor constraintEqualToAnchor: _pinSubTitleLabel.widthAnchor constant:1.0].active = YES;
    // Stops the opposite behavior of the label being much wider that the content of interest.
    [_pinTitleLabel.widthAnchor constraintLessThanOrEqualToAnchor: _pinTitle.widthAnchor constant: -75.0].active = YES;

    UIColor * textEntryBackgroundColor = [UIColor whiteColor];
    _pinTitle.backgroundColor = textEntryBackgroundColor;
    _pinSubTitle.backgroundColor = textEntryBackgroundColor;
}

- (void) layoutPinNotesFromAboveView: (UIView*) aboveView
{
    _pinNotesLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _pinNotes.translatesAutoresizingMaskIntoConstraints = NO;

    UIColor * textEntryBackgroundColor = [UIColor whiteColor];
    
    NSArray * tempStackSubviews = nil;
    UIStackView * tempStack = nil;
    _pinNotes.backgroundColor = textEntryBackgroundColor;
    
    tempStackSubviews =  @[_pinNotesLabel, _pinNotes];
    tempStack = [[UIStackView alloc] initWithArrangedSubviews: tempStackSubviews];
    tempStack.translatesAutoresizingMaskIntoConstraints = NO;
    tempStack.axis = UILayoutConstraintAxisHorizontal;
    tempStack.distribution = UIStackViewDistributionFill;
    tempStack.alignment = UIStackViewAlignmentLeading;
    tempStack.spacing = 8;
    [_stack addArrangedSubview: tempStack];

   // [_pinNotes.widthAnchor constraintEqualToAnchor: nil constant: 100.0].active = YES;
    [_pinNotes.heightAnchor constraintEqualToAnchor: nil constant: 50.0].active = YES;
    
    if (_displayTitles) {
        [_pinNotesLabel.widthAnchor constraintEqualToAnchor: _pinSubTitleLabel.widthAnchor constant:1.0].active = YES;
    }

}

#pragma mark - DJRAdjustPinMapViewDelegate
//- (void) updateCoordinate: (CLLocationCoordinate2D) pCoordinate
- (void) updateCoordinate: (CLLocationCoordinate2D) pCoordinate accuracy: (CGFloat) pHorizontalAccuracy;
{
    // Not used unless updateDelegateLocation: is called upon Save.
    _pinCoordinate = pCoordinate;
    _horizontalAccuracy = pHorizontalAccuracy;
}


#pragma mark - Calls to the Delegate
- (void) updateDelegateLocation: (id) caller
{
    if([self.delegate respondsToSelector:@selector(updateLocationCoordinate:)]) {
        [self.delegate updateLocationCoordinate: self];
    }
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (void) cancelUpdateOfDelegateLocation: (id) caller
{
    if([self.delegate respondsToSelector:@selector(cancelLocationCoordinateUpdate)]) {
        [self.delegate cancelLocationCoordinateUpdate];
    }
    [self dismissViewControllerAnimated: YES completion: nil];
}


#pragma mark - Template methods
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
