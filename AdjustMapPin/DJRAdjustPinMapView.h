//
//  DJRAdjustPinMapView.h
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

/*
 Due to use of UIStackViews, this is limited to iOS 9+
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface DJRAdjustPinMapView : MKMapView <MKMapViewDelegate>

@property CLLocationCoordinate2D initialCoordinate;
@property CLLocationCoordinate2D currentCoordinate;

@property id consumerDelegate;

- (instancetype) initWithCorordiate2D: (CLLocationCoordinate2D) pCoordinate;

@end

@protocol DJRAdjustPinMapViewDelegate <NSObject>
@optional
- (void) updateCoordinate: (CLLocationCoordinate2D) pCoordinate accuracy: (CGFloat) pHorizontalAccuracy;
@end
