//
//  DJRAdjustPinMapView.m
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

#import "DJRAdjustPinMapView.h"

@interface DJRAdjustPinMapView ()
@property MKPointAnnotation * annotationPoint;
//@property UITapGestureRecognizer * tapGesture;
@end

@implementation DJRAdjustPinMapView

- (instancetype) initWithCorordiate2D: (CLLocationCoordinate2D) pCoordinate
{
    self = [super init];
    if (self) {
        _initialCoordinate = pCoordinate;
        _currentCoordinate = pCoordinate;
        
        _annotationPoint = [MKPointAnnotation new];
        _annotationPoint.coordinate = _currentCoordinate;
        [self addAnnotation: _annotationPoint];
        
        self.delegate = self;
        
        MKCoordinateSpan span = {0.17,0.17};
        MKCoordinateRegion region = {_currentCoordinate, span};
        [self setRegion: region animated:true];

        UILongPressGestureRecognizer * tapGesture = [[UILongPressGestureRecognizer alloc] initWithTarget: self action: @selector(movePin:)];
        tapGesture.numberOfTouchesRequired = 1;
        tapGesture.minimumPressDuration = 0.5; // Require a half second to avoid false moves.
        [self addGestureRecognizer: tapGesture];
    }
    return self;
}


- (void) movePin:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView: self];
    _currentCoordinate = [self convertPoint: touchPoint toCoordinateFromView: self];
    
    if([self.consumerDelegate respondsToSelector:@selector(updateCoordinate:accuracy:)]) {
        [self.consumerDelegate updateCoordinate: _currentCoordinate accuracy: [self currentHorizontalAccuracy]];
    }

    // This seems to be the better animation of a moved pin.
    // I tried centering the map but if you leave your finger down,
    // more movement occurs that might not be wanted.
    [UIView animateWithDuration: 0.1
                     animations: ^{
                         _annotationPoint.coordinate = _currentCoordinate;
                     }
                     completion: nil];
}


- (CGFloat) currentHorizontalAccuracy // In meters.
{
    // latitudeDelta is degrees and "one degree of latitude is always approximately 111 kilometers".
    CGFloat accuracy = (self.region.span.latitudeDelta * 111.0 * 1000) / self.frame.size.height;

    // Looks interesting but I don't 'get' what the values actually mean. Meters?
//    MKMapRect mapRect = self.visibleMapRect;
//    CGRect mapInCGRect = CGRectMake(mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height);
//    NSLog(@"self.visibleMapRect: %@", NSStringFromCGRect(mapInCGRect));
    
    return accuracy;
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString * identifier = @"DJRPinAnnotView";
    MKPinAnnotationView * pinAnnotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: identifier];

    if (!pinAnnotationView)  {
        pinAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation: annotation
                                                            reuseIdentifier: identifier];
        pinAnnotationView.draggable = YES;
        pinAnnotationView.canShowCallout = NO;
        pinAnnotationView.animatesDrop = YES;
        
        // Weird! When set to NO, Allows dragging upon first tap & hold.
        // Weirder! BUT, we don't get the same pin animation and sharp end (as in Maps)!
        //pinAnnotationView.enabled = NO;

        if ([pinAnnotationView respondsToSelector:@selector(setPinTintColor:)]) {
            pinAnnotationView.pinTintColor = [MKPinAnnotationView purplePinColor];
        }
        else { // Pre-iOS 9
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            pinAnnotationView.pinColor = MKPinAnnotationColorPurple;
#pragma clang diagnostic pop
        }
    }

    return pinAnnotationView;
}


//- (void)mapView:(MKMapView *)mapView
//didSelectAnnotationView:(MKAnnotationView *)view
//{
//    NSLog(@"didSelectAnnotationView");
//    [view setDragState: MKAnnotationViewDragStateStarting animated: YES];
//}


- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    switch (newState) {
        case MKAnnotationViewDragStateEnding:
            _currentCoordinate = annotationView.annotation.coordinate;
            
            if([self.consumerDelegate respondsToSelector:@selector(updateCoordinate:accuracy:)]) {
                [self.consumerDelegate updateCoordinate: _currentCoordinate accuracy: [self currentHorizontalAccuracy]];
            }
            break;

//        case MKAnnotationViewDragStateNone:
//            NSLog(@"  MKAnnotationViewDragStateNone");
//            break;
//        case MKAnnotationViewDragStateStarting:
//            NSLog(@"  MKAnnotationViewDragStateStarting");
//            break;
//        case MKAnnotationViewDragStateDragging:
//            NSLog(@"  MKAnnotationViewDragStateDragging");
//            break;
//        case MKAnnotationViewDragStateCanceling:
//            NSLog(@"  MKAnnotationViewDragStateCanceling");
//            break;
            
        default:
            break;
    }
}
@end
