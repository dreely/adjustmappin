//
//  main.m
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
