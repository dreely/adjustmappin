//
//  DJRAdjustPinMapViewController.h
//  AdjustMapPin
//
//  Created by Darren Reely on 3/12/16.
//  Copyright © 2016 Darren Reely. All rights reserved.
//

/*
 Due to use of UIStackViews, this is limited to iOS 9+
 */

//#import <UIKit/UIKit.h>
//#import <Foundation/Foundation.h>
//#import <CoreLocation/CoreLocation.h>
//#import <MapKit/MapKit.h>

@import UIKit;
@import Foundation;
@import CoreLocation;
@import MapKit;

#import "DJRAdjustPinMapView.h"


@interface DJRAdjustPinMapViewController : UIViewController <DJRAdjustPinMapViewDelegate>

@property CLLocationCoordinate2D pinCoordinate;
@property CGFloat horizontalAccuracy;
@property id delegate;

@property (strong, nonatomic) IBOutlet UILabel * pinTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel * pinSubTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel * pinNotesLabel;

@property (strong, nonatomic) IBOutlet UITextField *pinTitle; // title occurs in UIViewControllers.
@property (strong, nonatomic) IBOutlet UITextField *pinSubTitle;
@property (strong, nonatomic) IBOutlet UITextView *pinNotes;

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy;

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              title: (NSString*) pTitle
                           subTitle: (NSString*) pSubTitle;

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              title: (NSString*) pTitle
                           subTitle: (NSString*) pSubTitle
                              notes: (NSString*) pNotes;

- (instancetype) initWithCoordinate: (CLLocationCoordinate2D) pCoordinate
                 horizontalAccuracy: (CGFloat) pHorizontalAccuracy
                              notes: (NSString*) pNotes;


- (void) updateDelegateLocation: (id) caller;
- (void) cancelUpdateOfDelegateLocation: (id) caller;

//- (void) updateCoordinate: (CLLocationCoordinate2D) pCoordinate;
- (void) updateCoordinate: (CLLocationCoordinate2D) pCoordinate accuracy: (CGFloat) pHorizontalAccuracy;
@end

@protocol DJRAdjustPinMapViewControllerDelegate <NSObject>
@optional
- (void) updateLocationCoordinate: (DJRAdjustPinMapViewController *) adjustPinMapViewController;
- (void) cancelLocationCoordinateUpdate;
@end


