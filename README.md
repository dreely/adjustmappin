# README #

A sample app that allows editing of CLLocationCoordinate2D values via a pin on a map, and a few text values.

Use of UIStackViews and new layout anchors limits this app to iOS9+.